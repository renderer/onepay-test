var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var queryString = require('query-string');

var SECURE_SECRET = "A3EFDFABA8653DF2342E8DAC29B51AF0";

function decodeHex(hex) {
    var arr = [];
    for (var i = 0; i < hex.length; i += 2) {
      arr.push(parseInt(hex.substr(i, 2), 16));
    }
    return arr;
}

function generateHash(params) {
  var fieldNames = Object.keys(params);
  fieldNames.sort();
  var fields = "";

  for (var i = 0; i < fieldNames.length; i++) {
    var fieldName = fieldNames[i];
    if(fieldName && fieldName.startsWith('vpc_')) {
      fields += fieldName + "=" + params[fieldName] + "&";
    }
  };

  fields = fields.slice(0, -1);

  var key = new Buffer(decodeHex(SECURE_SECRET));
  return crypto.createHmac('SHA256', key).update(fields).digest('hex').toUpperCase();
}

router.post('/pay', function(req, res, next) {
  var params = {
    "vpc_Version": 2,
    "vpc_Command": "pay",
    "vpc_Currency": "VND",
    "vpc_AccessCode": "D67342C2",
    "vpc_Merchant": "ONEPAY",
    "vpc_Locale": "vn",
    "vpc_ReturnURL": "http://localhost:3000/done",
    "vpc_MerchTxnRef": Date.now() + "",
    "vpc_OrderInfo": "order" + Date.now(),
    "vpc_Amount": req.body.amount || 1000,
    "vpc_TicketNo": req.connection.remoteAddress,
    "Title": "test"
  };
  var hash = generateHash(params);
  params['vpc_SecureHash'] = hash;
  fields = queryString.stringify(params);
  // res.send(fields);
  res.redirect("https://mtf.onepay.vn/onecomm-pay/vpc.op?" + fields);
});

router.get('/done', function(req, res, next) {
  var params = {};
  for(var i in req.query) {
    if(i != 'vpc_SecureHash') {
      params[i] = req.query[i];
    }
  }
  var hash = generateHash(params);
  if(hash != req.query['vpc_SecureHash']) {
    return res.send('Hash not valid');
  }
  res.send(req.query);
});

module.exports = router;